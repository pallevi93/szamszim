import hoomd
import hoomd.md
import numpy as np
import os

def getPositions(system):
	N = len(system.particles)
	M = np.zeros([N, 3])

	for i in range(N):
		p = system.particles[i]
		M[i,:] = p.position

	return M

def getVelocity(system):
	N = len(system.particles)
	V = np.zeros([N, 3])

	for i in range(N):
		p = system.particles[i]
		V[i,:] = p.velocity

	return V

os.mkdir("100")
os.chdir("100")

hoomd.context.initialize("");

sigma = 0.3345 # [nm]
epsilon = 1.04513 # [kJ/mol]
T = 100 # K (Liquid temperature)
k_B = 0.00831445986144858 # kJ/mol/Kelvin
kT = k_B * T
#kT=0.6
T2=60
T1=100
kT1= k_B * T1
kT2= k_B * T2
system=hoomd.init.create_lattice(unitcell=hoomd.lattice.sc(a=1.0), n=10);
nl = hoomd.md.nlist.cell();
lj = hoomd.md.pair.lj(r_cut=2.5*sigma, nlist=nl);
lj.pair_coeff.set('A', 'A', epsilon=epsilon, sigma=sigma);

hoomd.md.integrate.mode_standard(dt=0.005);
all = hoomd.group.all();
hoomd.md.integrate.langevin(group=all, kT=kT, seed=42);
#hoomd.md.integrate.langevin(group=all, kT=hoomd.variant.linear_interp([(0,kT1), (1e5, kT2)]), seed=42)
hoomd.analyze.log(filename="output.log",
                  quantities=['temperature','potential_energy', 'kinetic_energy', 'pressure'],
                  period=1,
                  overwrite=True);

step = 1e3 # Time steps
time = np.arange(0,1e5,step)

N = len(system.particles)
positionsInTime = np.zeros([N,3,len(time)])


for i in range(len(time)):
	M = getPositions(system)
	V = getVelocity(system)
	np.savetxt(str(i),M)
	np.savetxt(str(i)+"v",V)
	hoomd.run(step)


