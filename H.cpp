#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <time.h>
#include <algorithm>
#include <math.h>
#include <sys/stat.h>
#include <cstdlib>
#include <ctime>
#include <iterator>


using namespace std;
typedef vector<vector<double> > Matrix;

double DE(int xi, int xip1, int xim1)
	{
	double de;
	de = 2 * xi * (xim1 + xip1);
	return de;
	}
	
int XRand(int i)
	{
	int randomval = rand() % i;
	
	return randomval;
	}
	
double fRand(double fMin, double fMax)
	{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
	}
	
void SaveMatrix(int n, int m, string szo, Matrix& M)
	{
	ofstream file (szo.c_str());
	if (!file)
		{
		cerr << "Cannot open " << "d.dat" << "\nExiting ...\n";
		}
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m-1; ++l)
			{
			file << M[k][l] << '\t';
			}
		file << M[k][m-1] << '\n';
		}
	file.close();
	}
	
Matrix createMatrix(int n, int m)
	{
	Matrix eredmeny(n);
	
	for (int i = 0; i < n; ++i)
		eredmeny[i].resize(m);
	return eredmeny;
	}

int main()
	{
	srand(time(0));
	int N = 2;
	
	
	int P;
	
	
	chdir("1");
	
	
	while (N<100001)
		{
		Matrix H (N,vector <double>(N,0));
		for (int i = 1; i < N; ++i)
			{
		
			P = XRand(i);
			
			H[i][P] = 1;
			H[P][i] = 1;
			
			}
		cout << N << endl;
		SaveMatrix(N, N,"B_A,1," + to_string(N) + ".txt", H);

		N++;
		}
	
	return 0;
	}
