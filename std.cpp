#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <time.h>
#include <algorithm>
#include <sys/stat.h>
using namespace std;
typedef vector<vector<double> > Matrix;
//AVERAGE
double Average(vector<double> v)
{      double sum=0;
       for(int i=0;i<v.size();i++)
               {
               sum = sum + v[i];
               //cout << v[i] << "; " << sum << endl;
               }
       //cout << v[4] << "; " << v.size() << "; " << sum/v.size() << endl;
       return sum/v.size();
}
//DEVIATION
double Deviation(vector<double> v, double ave)
{
    double E=0;
    // Quick Question - Can vector::size() return 0?
    double inverse = 1.0 / static_cast<double>(v.size());
    for(unsigned int i=0;i<v.size();i++)
    {
        E += pow(static_cast<double>(v[i]) - ave, 2);
    }
    return sqrt(inverse * E);
}

double standard_deviation(vector<double> data, int n)
{
    double mean=0.0, sum_deviation=0.0;
    int i;
    for(i=0; i<n;++i)
    {
        mean+=data[i];
    }
    mean=mean/n;
    for(i=0; i<n;++i)
    sum_deviation+=(data[i]-mean)*(data[i]-mean);
    return sqrt(sum_deviation/n);           
}
   
   
void LoadVector(int n, string szo,vector<double>& v)
	{
	ifstream fajl (szo.c_str());
	if (!fajl.is_open())
		{
		cout << "nem sikerült megnyitni a fajlt!" << endl;
		}
	
	double szam;
	for (int k = 0; k < n; ++k)
		{
		fajl >> szam;
		v[k] = szam;
		//cout << v[k] << endl;
		}
	fajl.close();
	} 

void SaveMatrix(int n, int m, string szo, Matrix& M)
	{
	ofstream file (szo.c_str());
	if (!file)
		{
		cerr << "Cannot open " << "d.dat" << "\nExiting ...\n";
		}
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m-1; ++l)
			{
			file << M[k][l] << '\t';
			}
		file << M[k][m-1] << '\n';
		}
	file.close();
	}
Matrix createMatrix(int n, int m)
	{
	Matrix eredmeny(n);
	
	for (int i = 0; i < n; ++i)
		eredmeny[i].resize(m);
	return eredmeny;
	}
 
              
int main()
{
    int n = 10000000;
    chdir("30,10m");
    vector<double> v(n); //vector of length n
    Matrix M (28,vector <double>(5,0));
    for (int k = 0; k < 28; k++)
	{
	ostringstream asd;
	asd << k;
	string dsa = asd.str() + "riv2.dat";
    	LoadVector(n, dsa, v);
    	//cout << v[4] << endl;
    	double AVG = Average(v); 
    	double dev = Deviation(v,AVG); 
    	
    	M[k][0] = k+2;
    	M[k][1] = AVG;
    	M[k][2] = dev;
           M[k][3] = dev/sqrt(n);
           M[k][4] = AVG/(k+2);  
	}
    SaveMatrix(28, 5, "ste10m2.dat", M);
    
}
