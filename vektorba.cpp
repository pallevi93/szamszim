#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <time.h>
#include <algorithm>
#include <sys/stat.h>

// g++ -std=c++0x dens.cpp 

using namespace std;
typedef vector<vector<double> > Matrix;
typedef vector<vector<vector<double> > > ThDMtx;


void LoadMatrix(int n, int m, string szo, Matrix& M)
	{
	ifstream fajl (szo.c_str());
	if (!fajl.is_open())
		{
		cout << "nem sikerült megnyitni a fajlt!" << endl;
		}
	
	double szam;
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m; ++l)
			{
			fajl >> szam;
			M[k][l] = szam;
			//cout << M[k][l] << "; " << k << "; " << l << endl;
			}
		}
	fajl.close();
	}
	
void SaveMatrix(int n, int m, string szo, Matrix& M)
	{
	ofstream file (szo.c_str());
	if (!file)
		{
		cerr << "Cannot open " << "d.dat" << "\nExiting ...\n";
		}
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m-1; ++l)
			{
			file << M[k][l] << '\t';
			}
		file << M[k][m-1] << '\n';
		}
	file.close();
	}
Matrix createMatrix(int n, int m)
	{
	Matrix eredmeny(n);
	
	for (int i = 0; i < n; ++i)
		eredmeny[i].resize(m);
	return eredmeny;
	}


/*3DMtx create3DMtx(int n, int m, int k)
	{
	
	}*/

int main()
	{
	clock_t t0 = clock();
	clock_t td;
	
	chdir("2,300,20e");
	Matrix M1 (298,vector <double>(20000,0));
	Matrix M2 (298,vector <double>(20000,0));
	//vector<double> v(20000);
	for (int i = 0; i < 20000; i++)
		{
		Matrix r (298,vector <double>(3,0));
		string asd = to_string(i) + "ri.dat";
		LoadMatrix(298, 3, asd, r);
		
		for (int j = 0; j < 298; j++)
			{
			M1[j][i] = r[j][1];
			M2[j][i] = r[j][2];
			}
		}
		
	chdir("..");
	chdir("300,20e");
	
	
	for (int k = 0; k < 298; k++)
		{
		ostringstream asd;
		asd << k;
		string dsa = asd.str() + "riv1.dat";
		ofstream file(dsa.c_str());
		if (!file)
			{
			cerr << "Cannot open " << "d.dat" << "\nExiting ...\n";
			return 1;
			}
		
		for (int ii = 0; ii < 20000; ii++)
			{
			file << M1[k][ii] << endl;
			}
		file.close();
		
		string dsa2 = asd.str() + "riv2.dat";
		ofstream file2(dsa2.c_str());
		if (!file2)
			{
			cerr << "Cannot open " << "d.dat" << "\nExiting ...\n";
			return 1;
			}
		for (int jj = 0; jj < 20000; jj++)
			{
			file2 << M2[k][jj] << endl;
			}
		file2.close();
			
		
		}	
	
	
		
	td = clock() - t0;
	cout << ((double)td)/CLOCKS_PER_SEC << endl;
	return 0;
	}
