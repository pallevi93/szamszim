#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <time.h>
using namespace std;

double omega;          // the natural frequency
double x, v;           // position and velocity at time t
int periods;           // number of periods to integrate
int stepsPerPeriod;    // number of time steps dt per period
string fileName;       // name of output file

void getInput();                 // for user to input parameters
void EulerCromer(double dt);     // takes an Euler-Cromer step
double energy();                 // computes the energy

void getInput ( ) {
    cout << "Enter omega: ";
    cin >> omega;
    cout << "Enter x(0) and v(0): ";
    cin >> x >> v;
    cout << "Enter number of periods: ";
    cin >> periods;
    cout << "Enter steps per period: ";
    cin >> stepsPerPeriod;
    cout << "Enter output file name: ";
    cin >> fileName;
}

void EulerCromer (double dt) {
    double a = - omega * omega * x;
    v += a * dt;
    x += v * dt;
}

void Euler (double dt) {
    double a = - omega * omega * x;
    x += v * dt;
    v += a * dt;
}

double energy ( ) {
    return 0.5 * (v * v + omega * omega * x * x);
}

int main ( ) {

    getInput();

    ofstream file(fileName.c_str());
    if (!file) {
        cerr << "Cannot open " << fileName << "\nExiting ...\n";
        return 1;
    }

    const double pi = 4 * atan(1.0);
    double T = 2 * pi / omega;
    double dt = T / stepsPerPeriod;
    double t = 0;

    file << t << '\t' << x << '\t' << v << '\n';
    
    int sp;
    clock_t t0;
    clock_t td;
    t0 = clock();
    for (int p = 1; p <= periods; p++) {

        for (int s = 0; s < stepsPerPeriod; s++) {
            Euler(dt);
            //EulerCromer(dt);
            t += dt;
            td = clock() - t0;
            sp = (((p-1)*stepsPerPeriod)+s);
            file << t << '\t' << x << '\t' << v << '\t' << energy() << '\t' << sp << '\t' << ((double)td)/CLOCKS_PER_SEC << '\t' << '\n';
            
        }
        
        /*cout << "Period = " << p << "\tt = " << t
             << "\tx = " << x << "\tv = " << v
             << "\tenergy = " << energy() << endl << ((double)td)/CLOCKS_PER_SEC <<endl;*/
    }

    file.close();
}

