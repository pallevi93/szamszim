#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include "vector.hpp"        // vectors with components of type double
#include "odeint.hpp"        // ODE integration routines, Runge-Kutta ...
using namespace cpl;

const double pi = 4 * atan(1.0);

const double g = 9.8;        // acceleration of gravity

double L = 1.0;              // length of pendulum
double q = 0.5;              // damping coefficient
double Omega_D = 2.0/3.0;    // frequency of driving force
double F_D = 0.9;            // amplitude of driving force
bool nonlinear;              // linear if false

Vector f(const Vector& x) {  // extended derivative vector
    double t = x[0];
    double theta = x[1];
    double omega = x[2];
    Vector f(3);             // Vector with 3 components
    f[0] = 1;
    f[1] = omega;
    if (nonlinear)
        f[2] = - (g/L) * sin(theta) - q * omega + F_D * sin(Omega_D * t);
    else
        f[2] = - (g/L) * theta - q * omega + F_D * sin(Omega_D * t);
    return f;
}

double energy (double theta, double omega) {
    return (0.5 * (L * L * omega * omega)) + (g * L * (1 - cos(theta)));
}

void EulerCromer (double dt, double& t, double& theta, double& omega) {
    double a;
    if (nonlinear)
        a = - (g/L) * sin(theta) - q * omega + F_D * sin(Omega_D * t);
    else
        a = - (g/L) * theta - q * omega + F_D * sin(Omega_D * t);
    
    omega += a * dt;
    theta += omega * dt;
    t += dt;
}

void Euler (double dt, double& t, double& theta, double& omega) {
    double a;
    if (nonlinear)
        a = - (g/L) * sin(theta) - q * omega + F_D * sin(Omega_D * t);
    else
        {
        a = - (g/L) * theta - q * omega + F_D * sin(Omega_D * t);
        //cout << a << endl;
        }
    theta += omega * dt;
    omega += a * dt;
    t += dt;
}

void DoubPend (double dt, double& t, double& theta1, double& omega1, double& theta2, double& omega2) {
    
    double a1 = (-3 * g * sin(theta1) - g * sin(theta1 - 2 * theta2) - 2 * sin(theta1 - theta2) * (omega2 * omega2 * L + omega1 * omega1 * L * cos(theta1 - theta2))) / (L * (3 - cos(2 * theta1 - 2 * theta2)));
    
    double a2 = (2 * sin(theta1 - theta2) * (omega1 * omega1 * L * 2 + g * 2 * cos(theta1) + omega2 * omega2 * L * cos(theta1 - theta2))) / (L * (3 - cos(2 * theta1 - 2 * theta2)));
    
    omega1 += a1 * dt;
    theta1 += omega1 * dt;
    omega2 += a2 * dt;
    theta2 += omega2 * dt;
    t += dt;
}

int main() {
    cout << " Nonlinear damped driven pendulum\n"
         << " --------------------------------\n"
         << " Enter linear or nonlinear: ";
    string response;
    cin >> response;
    nonlinear = (response[0] == 'n');
    cout<< " Length of pendulum L: ";
    cin >> L;
    cout<< " Enter damping coefficient q: ";
    cin >> q;
    cout << " Enter driving frequencey Omega_D: ";
    cin >> Omega_D;
    cout << " Enter driving amplitude F_D: ";
    cin >> F_D;
    cout << " Enter theta(0) and omega(0): ";
    double theta, omega, tMax;
    cin >> theta >> omega;
    cout << " Enter integration time t_max: ";
    cin >> tMax;

    double dt = 0.05;
    double accuracy = 1e-6;
    ofstream dataFile("eucfiz.dat");
    
    double theta1 = 3;
    double omega1 = 0;
    double theta2 = 0;
    double omega2 = 0;
    
    double t = 0;
    Vector x(3);
    x[0] = t;
    x[1] = theta;
    x[2] = omega;

    while (t < tMax) {
        //adaptiveRK4Step(x, dt, accuracy, f);
        //RK4Step(x, dt, f);
        //Euler (dt, t, theta, omega);
        EulerCromer (dt, t, theta, omega);
        //DoubPend (dt, t, theta1, omega1, theta2, omega2);
        
        //t = x[0], theta = x[1], omega = x[2];
        if (nonlinear) {
            while (theta >= pi) theta -= 2 * pi;
            while (theta < -pi) theta += 2 * pi;
        }
        dataFile << t << '\t' << theta << '\t' << omega << '\t' << energy(theta, omega) << '\n';
        //dataFile << t << '\t' << theta1 << '\t' << omega1 << '\t' << theta2 << '\t' << omega2 << '\n';
    }

    cout << " Output data to file pendulum.data" << endl;
    dataFile.close();
}

