#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <time.h>
#include <algorithm>
#include <math.h>
#include <sys/stat.h>
#include <cstdlib>
#include <ctime>
#include <iterator>

using namespace std;
typedef vector<vector<double> > Matrix;

double DE(int X0, int x)
	{
	double de;
	de = (((X0 + x) * (X0 + x)) - (X0 * X0));
	return de;
	}
	
int XRand()
	{
	int x;
	int randomval = rand() % 2;
	if (randomval == 0)
		x = -1;
	else
		x = 1;
	return x;
	}
	
double fRand(double fMin, double fMax)
	{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
	}
	
void SaveMatrix(int n, int m, string szo, Matrix& M)
	{
	ofstream file (szo.c_str());
	if (!file)
		{
		cerr << "Cannot open " << "d.dat" << "\nExiting ...\n";
		}
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m-1; ++l)
			{
			file << M[k][l] << '\t';
			}
		file << M[k][m-1] << '\n';
		}
	file.close();
	}
	
Matrix createMatrix(int n, int m)
	{
	Matrix eredmeny(n);
	
	for (int i = 0; i < n; ++i)
		eredmeny[i].resize(m);
	return eredmeny;
	}
	
int main()
	{
	srand(time(0));
	int N = 100000000;
	int X0 = 0;
	double P;
	double bka2 = 0.2;
	double xv = 0;
	double xv2 = 0;
	
	/*Matrix r = createMatrix(N, 2);
	r[0][0] = 0;
	r[0][1] = 0;*/
	vector<int> r;
	
	for (int n = 1; n < N; n++)
		{
		int x = XRand();
		//cout << x << endl;
		//cout << (bka2 * (-0.5) * DE(n, x))<< "pr" << endl;
		
		if (DE(X0, x) < 0)
			{
			//cout << x << " " << DE(n, x) << endl;
			X0 = X0 + x;
			}
		else
			{
			P = fRand(0, 1);
			//cout << P << endl;
			//cout << "exp" << exp(bka2 * (-0.5) * DE(X0, x)) << endl;
			if (P < (exp(bka2 * (-0.5) * DE(X0, x))))
				{
				X0 = X0 + x;
				}
			else
				{
				X0 = X0;
				//cout << "asd" << X0 << endl;
				}
			}
		//r[n][0] = n;
		r.push_back(X0);
		//r[n][1] = X0;
		xv = xv + X0;
		xv2 = xv2 + (X0 * X0);
		}
	int i = -10;
	int j = 0;
	Matrix v = createMatrix(21, 2);
	while (i < 11)
		{
		double s = count(r.begin(), r.end(), i);
		v[j][1] = s / 100000000 ;
		v[j][0] = i;
		i++;
		j++;
		}
	xv = xv/N;
	xv2 = xv2/N;
	double s2 = xv2 - (xv*xv);
	double s = sqrt(s2);
	cout << xv << "; " << xv2 << "; " << s2 << "; " << s << endl;
	//SaveMatrix(N, 2, "mc.dat", r);
	SaveMatrix(21, 2, "el02.dat", v);
	return 0;
	}
