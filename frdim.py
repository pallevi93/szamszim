import numpy as np
import pylab as pl

def rgb2gray(rgb):   #szurkearnylatuva konvertalom a kepet nincs rgb dimenzio
	r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
	gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
	return gray

def box_count(image, size):
	box_value = np.add.reduceat(np.add.reduceat(image, np.arange(0, image.shape[0], size), axis=0), np.arange(0, image.shape[1], size), axis=1)
	#a kepet felosztom boxokra es az egyes boxoban levo pixelek erteket osszeadom True=1 False=0 
	#print S	
	return len(np.where((box_value > 0) & (box_value < size*size))[0]) #visszaadja azon boxok szamat ami nem ures es nem teli vagyis nem csupa igaz vagy csupa hamis

def fractal_dimension(image, threshold=50):
	#print image
	#print image.shape
	#pl.imshow(image)
	#pl.show()
	image = (image < threshold) #ha a kep adott pixelenek erteke kisebb mint a megadott hatar akkor a pixelhez igazat rendel
	#print image
	#pl.imshow(image)
	#pl.show()
	#pl.imsave("out2.png", image)
	
	l_min = min(image.shape) #a max boxhoz vesszuk a kep rovidebb oldalat
	n = 2**np.floor(np.log(l_min)/np.log(2)) #keressuk a legnagyobb 2 hatvanyt ami meg van benne
	n = int(np.log(n)/np.log(2)) #hanyadik ez a hatvany
	
	sizes = 2**np.arange(n, 1, -1) #a hasznalando boxok meretei, csokkeno sorrendben 2 hatvanyai
	counts = []
	for size in sizes:
		#print boxcount(image, size), size
		counts.append(box_count(image, size))  #vektorba rendezem h melyik boxmerethez mennyi olyan box volt ami nem ures vagy teli
		
	#print counts
	coeffs = np.polyfit(np.log(sizes), np.log(counts), 1)  #fittelek a box meret logaritmusa fv ben a 
	#pl.plot(np.log(sizes),np.log(counts), 'o', mfc='none')
	#pl.plot(np.log(sizes), np.polyval(coeffs,np.log(sizes)))
	#pl.xlabel('log $\epsilon$')
	#pl.ylabel('log N')
	#pl.savefig(nev+'.pdf')
	
	np.savetxt(nev+',txt', np.column_stack((sizes,counts)))
	return -coeffs[0]
	
nev='fa2'	
pl.gray()
I = rgb2gray(pl.imread(nev+'.jpg'))   #beolvasom a kepet, meg lefuttatom a fvem
print fractal_dimension(I)
