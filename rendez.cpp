#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <fstream>
#include <algorithm>
#include <list>
#include <sys/stat.h>
#include <string>
#include <unistd.h>

using namespace std;

struct sortclass{
	bool operator()(int i, int j){ return (i<j); }
} myobject_sort;

int main()
{	
mode_t mt;
mkdir("2,300,20e", mt);
chdir("2,300,20e");

for (int i = 0; i < 20000; i++)
	{
	//cout << i << endl;
	string asd = to_string(i) + "ri.dat";
	ofstream file(asd.c_str());
	for (int max = 2; max < 300; max++)
		{
		vector<int> v; // ide ker�lnek a sz�mok
		vector<int> v2;
		int szam = 0;
		
		
		//int max = 100000;


		srand(time(NULL)); //kezdeti �rt�k az id� lesz a pszeudov�letlennek
		for(int i=0; i<max; i++){
			szam = rand();
			v.push_back(szam); //itt j�n l�tre a vektor
			v2.push_back(szam);
		}

		/*for(int i=0; i < v.size(); ++i){
			cout << v[i] <<endl;
		}
		for(int i=0; i < v.size(); ++i){
			cout << v2[i] <<endl;
		}*/

		clock_t start = clock(); //id�m�r�s kezdete

		sort(v.begin(), v.end());

		clock_t stop = clock(); //id�m�r�s v�ge


		/*for(int i=0; i < v.size(); ++i){
			cout << v[i] <<endl;
		}*/

		double t = (double)(stop-start);
		//cout << "sort: "<< max << '\t' << t <<endl;
		//file << max << '\t' << t <<endl;
		//}

		int c, d, swap;

		clock_t start2 = clock(); //id�m�r�s kezdete

		for (c = 0 ; c < ( max - 1 ); c++)
		  {
		    for (d = 0 ; d < max - c - 1; d++)
		    {
		      if (v2[d] > v2[d+1]) /* For decreasing order use < */
		      {
		        swap       = v2[d];
		        v2[d]   = v2[d+1];
		        v2[d+1] = swap;
		      }
		    }
		  }

		clock_t stop2 = clock();
		double t2 = (double)(stop2-start2);
		
		
		/*for(int i=0; i < v.size(); ++i){
			cout << v2[i] <<endl;
		}*/

		//cout << "Bubble: " << max << '\t' << t2 <<endl;
		
		file << max << '\t' << ((double)t)/CLOCKS_PER_SEC << '\t' << ((double)t2)/CLOCKS_PER_SEC <<endl;
		}
	file.close();
	}
return 0;
}
