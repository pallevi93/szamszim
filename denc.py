#!/usr/bin/env python
#coding: utf8 
#a felette lévő cuccok a kommenteléshez kellenek valamiért.. hibát dobott ki eddig..
from __future__ import division #osztás miatt
import math #cos sin pi ....
import random #random szám generálás
import os #mappa kezelés
import argparse #kapcsoló
import time #idő számláló
from math import sqrt
import sys   #  kilépés a program hoz.. sys.exit(0)
import subprocess as sp  # bash script futtatás
import numpy as np

os.chdir("60")
step=1e5
kiir=1e3

N=1000
L=10
dt=0.005


cp=[[0,0] for x in range(0,551)]

DENS = np.zeros((1,N,N))


def tav(r,Lx,Ly,Lz):
	ez=sqrt((r[j][0]+Lx-r[i][0])**2 +(r[j][1]+Ly-r[i][1])**2+(r[j][2]+Lz-r[i][2])**2)
	return ez



g=0

	
h = open ( "99" , 'r')
r = [ map(float,line.split(' ')) for line in h ] #beolvasom az adatokat egy listába, 2 indexes az eslő megadja h melyik pont a 2. meg h x krd vagy y krd vagy a theta
h.close()
print r[1][0], r[1][1], r[1][2]
	
for i in range (0,N):
		
	for j in range(0,N):
		if i!=j:
			DENS[g][i][j]=min(tav(r,0,0,0),tav(r,L,0,0),tav(r,-L,0,0),tav(r,0,L,0),tav(r,0,-L,0),tav(r,0,0,L),tav(r,0,0,-L),tav(r,L,L,0),tav(r,L,-L,0),tav(r,L,0,L),tav(r,L,0,-L),tav(r,-L,L,0),tav(r,-L,-L,0),tav(r,-L,0,L),tav(r,-L,0,-L),tav(r,0,L,L),tav(r,0,L,-L),tav(r,0,-L,L),tav(r,0,-L,-L),tav(r,L,L,L),tav(r,-L,L,L),tav(r,L,-L,L),tav(r,L,L,-L),tav(r,-L,-L,L),tav(r,-L,L,-L),tav(r,L,-L,-L),tav(r,-L,-L,-L))
			#print DENS[g][i][j]
q=0
rad=0
delta=0.05
while (rad<10)	:	
	corr=0	
	for i in range (0,N):
		
		for j in range(0,N):
			if i!=j:
				if DENS[g][i][j] >= rad and DENS[g][i][j]< delta+rad:
					corr=corr+1
	cp[q][1]=corr/(N*(N-1))
	cp[q][0]=rad				
	rad=rad+delta
	q=q+1

np.savetxt('denscr2.dat',cp)
