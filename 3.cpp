#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <time.h>
#include <algorithm>
#include <math.h>
#include <sys/stat.h>
#include <cstdlib>
#include <ctime>
#include <iterator>

using namespace std;
typedef vector<vector<int> > Matrix;

int XRand()
	{
	int x;
	int randomval = rand() % 2;
	if (randomval == 0)
		x = 0;
	else
		x = 1;
	return x;
	}
	
void LoadMatrix(int n, int m, string szo, Matrix& M)
	{
	ifstream fajl (szo.c_str());
	if (!fajl.is_open())
		{
		cout << "nem sikerült megnyitni a fajlt!" << endl;
		}
	
	double szam;
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m; ++l)
			{
			fajl >> szam;
			M[k][l] = szam;
			//cout << M[k][l] << "; " << k << "; " << l << endl;
			}
		}
	fajl.close();
	}
	
void SaveMatrix(int n, int m, string szo, Matrix& M)
	{
	ofstream file (szo.c_str());
	if (!file)
		{
		cerr << "Cannot open " << "hahah.dat" << "\nExiting ...\n";
		}
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m-1; ++l)
			{
			file << M[k][l] << '\t';
			}
		file << M[k][m-1] << '\n';
		}
	file.close();
	}


Matrix createMatrix(int n, int m)
	{
	Matrix eredmeny(n);
	
	for (int i = 0; i < n; ++i)
		eredmeny[i].resize(m);
	return eredmeny;
	}
	
int szomszed(int i, int j, Matrix r)
	{
	int szomszed = 0;
	szomszed = r[i+1][j] + r[i-1][j] + r[i][j+1] + r[i][j-1] + r[i+1][j+1] + r[i+1][j-1] + r[i-1][j+1] + r[i-1][j-1];
	
	return szomszed;
	}
	
void SaveMatrix2(int n, int m, string szo, Matrix& M)
	{
	ofstream file (szo.c_str());
	if (!file)
		{
		cerr << "Cannot open " << "hahah.dat" << "\nExiting ...\n";
		}
	for (int k = 1; k < n+1; ++k)
		{
		for (int l = 1; l < m; ++l)
			{
			file << M[k][l] << '\t';
			}
		file << M[k][m] << '\n';
		}
	file.close();
	}
	
int main()
	{
	srand(time(0));
	
	int N = 21;
	Matrix r (N,vector <int>(N,0));
	
	
	mode_t mt;
	string mappa = "sandr";
	mkdir(mappa.c_str(), mt);
	chdir(mappa.c_str());
	
	int l =1;
	SaveMatrix(N,N,"0.dat",r);
	for (int k=1; k < 500; k++)
		{
		//r[7][7] = r[7][7]+1;
		int e = rand() % 15 +3;
		int f = rand() % 15 +3;
		r[e][f] = r[e][f] + 1;
		
		SaveMatrix(N,N, to_string(l) + ".dat",r);
		l++;
		for (int i = 0; i < N; i++)
			{
			for (int j = 0; j < N; j++)
				{
				if (r[i][j] >= 4)
					{
					r[i][j] = r[i][j] - 4;
					
					r[i+1][j] = r[i+1][j]+1;
					r[i][j+1] = r[i][j+1]+1;
					r[i-1][j] = r[i-1][j]+1;
					r[i][j-1] = r[i][j-1]+1;
					
					SaveMatrix(N,N, to_string(l) + ".dat",r);
					++l;
					i = i-1;
					j= j-1;
					}
				}
			}

		}
	return 0;
	}
