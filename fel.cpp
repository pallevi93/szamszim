#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <time.h>
using namespace std;
int main()
	{
	srand(time(0));
	
	double mu=100;
	double k=2;
	double D=0.1;
	double dt=0.00001;
	int N=10000000;

	default_random_engine generator;
	normal_distribution<double> distribution(0,sqrt(2*D*dt));

	double x = 0;
	ofstream dataFile("0.00001,2.txt");
	dataFile << x << '\n';
	
	for (int i=0; i<N; i++ )
		{
		
		double number = distribution(generator);
		//cout << i << "; " << number<< endl;
		x = x - mu * k * x * dt + number;
		dataFile << x << '\n';
		}
	
	dataFile.close();
	return 0;
	}
