#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <sstream>
#include <unistd.h>

using namespace std;
int main()
	{
	double R = 500;
	double F = 500;

	double a = 0.4;
	double b = 0.004;
	double c = 0.004;
	double d = 0.9;
	
	double K = 800;
	double S = 2500;
	double t = 0;
	double dt = 0.02;


	ofstream file( "10.txt");
	
	
	file << t << "\t" << R << "\t" << F << endl;
	
	while (t < 100)
		{
		t = t + dt;
		R = R + (a*(1-R/K) * R - b * (R * F/(1+R/S))) * dt;
		F = F + (-d * F + c * ((F * R)/(1+R/S))) * dt;
		
		file << t << "\t" << R << "\t" << F << endl;
		}
	file.close();
	
	
	}
