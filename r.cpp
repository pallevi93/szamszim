#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include "vector.hpp"        // vectors with components of type double
#include "odeint.hpp"        // ODE integration routines, Runge-Kutta ...
using namespace cpl;

const double pi = 4 * atan(1.0);
Vector f(const Vector& x) {
	double t = x[0], x0 = x[1], r=1;
	Vector f(2);
	f[0] = 1;
	f[1] = r*x0*(1-x0);
	return f;
}
int main() {
	string FileName;
	double r = 1;
	double x0 = 0.2;
	
	double t = 0;
	double dt = 0.1;
	
	double accuracy = 1e-6;
	ofstream dataFile("asd.txt");
	Vector x(2);
	x[0] = t;
	x[1] = x0;
	while (t < 10) {
		adaptiveRK4Step(x, dt, accuracy, f);
		t = x[0], x0 = x[1];
		dataFile << t << "\t" << x0 << "\n";
	}
	dataFile.close();
}
