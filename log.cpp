#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <sstream>
#include <unistd.h>

using namespace std;
int main()
	{
	double r = 1.5;
	double x = 0;
	
	double t = 0;
	double dt = 0.1;
	double x0 = 0;
	
	mode_t mt;
	ostringstream strs2;
	strs2 << r;
	string mappa = strs2.str();
	mkdir(mappa.c_str(), mt);
	chdir(mappa.c_str());
	
	while (x0 < 1.01)
		{
		ostringstream strs;
		strs << x;

		
		ofstream file(strs.str() + ".txt");
		
		file << t << "\t" << x << endl;
		
		while (t < 10)
			{
			t = t + dt;
			x = x + dt * r * x * (1 - x);
			file << t << "\t" << x << endl;
			}
		file.close();
		
		t = 0;
		x0 = x0 + 0.05;
		x = x0;
		}
	}
