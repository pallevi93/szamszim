#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <time.h>
#include <algorithm>
#include <math.h>
#include <sys/stat.h>
#include <cstdlib>
#include <ctime>
#include <iterator>

using namespace std;
typedef vector<vector<int> > Matrix;

int XRand()
	{
	int x;
	int randomval = rand() % 2;
	if (randomval == 0)
		x = 0;
	else
		x = 1;
	return x;
	}
	
void LoadMatrix(int n, int m, string szo, Matrix& M)
	{
	ifstream fajl (szo.c_str());
	if (!fajl.is_open())
		{
		cout << "nem sikerült megnyitni a fajlt!" << endl;
		}
	
	double szam;
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m; ++l)
			{
			fajl >> szam;
			M[k][l] = szam;
			//cout << M[k][l] << "; " << k << "; " << l << endl;
			}
		}
	fajl.close();
	}
	
void SaveMatrix(int n, int m, string szo, Matrix& M)
	{
	ofstream file (szo.c_str());
	if (!file)
		{
		cerr << "Cannot open " << "hahah.dat" << "\nExiting ...\n";
		}
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m-1; ++l)
			{
			file << M[k][l] << '\t';
			}
		file << M[k][m-1] << '\n';
		}
	file.close();
	}


Matrix createMatrix(int n, int m)
	{
	Matrix eredmeny(n);
	
	for (int i = 0; i < n; ++i)
		eredmeny[i].resize(m);
	return eredmeny;
	}
	
int szomszed(int i, int j, Matrix r)
	{
	int szomszed = 0;
	szomszed = r[i+1][j] + r[i-1][j] + r[i][j+1] + r[i][j-1] + r[i+1][j+1] + r[i+1][j-1] + r[i-1][j+1] + r[i-1][j-1];
	
	return szomszed;
	}
	
void SaveMatrix2(int n, int m, string szo, Matrix& M)
	{
	ofstream file (szo.c_str());
	if (!file)
		{
		cerr << "Cannot open " << "hahah.dat" << "\nExiting ...\n";
		}
	for (int k = 1; k < n+1; ++k)
		{
		for (int l = 1; l < m; ++l)
			{
			file << M[k][l] << '\t';
			}
		file << M[k][m] << '\n';
		}
	file.close();
	}
	
int main()
	{
	srand(time(0));
	//Matrix r = createMatrix(30, 30);
	int N = 100;
	Matrix r (N+2,vector <int>(N+2,0));
	Matrix r2 (N+2,vector <int>(N+2,0));
	int sz;
	int n = 2;
	
	mode_t mt;
	string mappa = "per";
	mkdir(mappa.c_str(), mt);
	chdir(mappa.c_str());
	
	/*for (int i = 0; i < N+2; i++)
		{
		for (int j = 0; j < N+2; j++)
			{
			if (i==0 || j==0 || i == (N+1) || j==(N+1))
				r[i][j] = XRand();
				//r[i][j] = 1;
			}
		}*/
	for (int i = 0; i < N+2; i++)
		{
		if (i%2 ==0)
			{
			r[i][0] = 1;
			r[i+1][N+1] = 1;
			}
		}
	for (int j = 0; j < N+2; j++)
		{
		if (j%2 ==0)
			{
			r[0][j] = 1;
			r[N+1][j+1] = 1;
			}
		}

	for (int i = 1; i < N+1; i++)
		{
		for (int j = 1; j < N+1; j++)
			{
			if (i==j)
				r[i][j]=1;
			if (i==(N+1-j))
				r[i][j]=1;
			if (i>=24 && i<= 37 && j>=24 && j<=37)
				r[i][j]=1;
			}
		}
	
	/*
	for (int i = 1; i < N+1; i++)
		{
		for (int j = 1; j < N+1; j++)
			{
			r[i][j] = XRand();
			}
		}
	*/	
	//SaveMatrix2(N,N,"0.dat",r);
	SaveMatrix(N+2,N+2,"0.dat",r);
	for (int k=1; k < 100; k++)
		{
		for (int i = 1; i < N+1; i++)
			{
			for (int j = 1; j < N+1; j++)
				{
				sz = szomszed(i, j, r);
				//cout << sz << endl;
				if (sz == n)
					{
					r2[i][j]=r[i][j];
					}
				else if (sz == (n+1))
					{
					r2[i][j]=1;
					}
				else
					r2[i][j]=0;
				}
			}
		for (int i = 1; i < N+1; i++)
			{
			for (int j = 1; j < N+1; j++)
				{
				r[i][j]=r2[i][j];
				}
			}
				
		//SaveMatrix2(N,N, to_string(k) + ".dat",r);
		SaveMatrix(N+2,N+2, to_string(k) + ".dat",r);
		}
	return 0;
	}
