#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <time.h>
#include <algorithm>
#include <math.h>
#include <sys/stat.h>
#include <cstdlib>
#include <ctime>
#include <iterator>


using namespace std;
typedef vector<vector<double> > Matrix;

double DE(int xi, int xip1, int xim1)
	{
	double de;
	de = 2 * xi * (xim1 + xip1);
	return de;
	}
	
int XRand(int i)
	{
	int randomval = rand() % i;
	
	return randomval;
	}
	
double fRand(double fMin, double fMax)
	{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
	}
	
void SaveMatrix(int n, int m, string szo, Matrix& M)
	{
	ofstream file (szo.c_str());
	if (!file)
		{
		cerr << "Cannot open " << "d.dat" << "\nExiting ...\n";
		}
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m-1; ++l)
			{
			file << M[k][l] << '\t';
			}
		file << M[k][m-1] << '\n';
		}
	file.close();
	}
	
Matrix createMatrix(int n, int m)
	{
	Matrix eredmeny(n);
	
	for (int i = 0; i < n; ++i)
		eredmeny[i].resize(m);
	return eredmeny;
	}

int main()
	{
	srand(time(0));
	int N = 1000;
	
	
	int P;
	
	double A;
	chdir("2");
	vector<double> vfoksz;
	
	while (N<1001)
		{
		Matrix H (N,vector <double>(N,0));
		for (int i = 1; i < N; ++i)
			{
			cout << i << endl;
			double asd = 0;
			while (asd == 0)
				{
				int k = 0;
				A = 0;
				P = XRand(i);
			
				for (int j = 0; j < N; j++)
					{
					if (P!=j)
						{
						if (H[P][j] == 1)
							{
							k++;
							}
						}
					}
			
				for (int ii = 0; ii < N; ii++)
					{
					double foksz = 0;
					for (int jj = 0; jj < N; jj++)
						{
						if (ii!=jj)
							{
							if (H[ii][jj] == 1)
								{
								foksz++;
								}
							}
						}
					vfoksz.push_back (foksz);
					}
				for (int kk = 1; kk <= N-1; kk++)
					{
					double z = count (vfoksz.begin(), vfoksz.end(), kk);
					A = A + z/kk;
					}
				double P2 = fRand(0, 1);
				//cout <<k <<" "<< A<< endl;
				//cout << (1/(A*k)) << endl;
				if (P2 < (1/(A*k)))
					{
				
					H[i][P] = 1;
					H[P][i] = 1;
					asd = 1;
					}
				vfoksz.clear();
				}
			}
		cout << N << endl;
		SaveMatrix(N, N,"AntPr," + to_string(N) + ".txt", H);
		
		N++;
		}
	
	return 0;
	}
