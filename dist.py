import numpy as np
from geopy.distance import vincenty
from geopy.distance import great_circle
import pandas as pd

my_cols=['A','B','C','D']



f='usa'

url=np.array(pd.read_csv(f+'.out', sep=' ', names=my_cols, engine='python'))

s = [[0,0,0] for s in range(0,len(url))]

my_cord = (47.500000, 19.083300)

for i in range(0,len(url)):
	other_cord=(float(url[i][2]), float(url[i][3]))
#	s[i][2] = (vincenty(my_cord, other_cord).miles)
	s[i][2] = (great_circle(my_cord, other_cord).miles)
	s[i][0] = url[i][0]
	s[i][1] = url[i][1]
	
np.savetxt(f+'.dat',s)
