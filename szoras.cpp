#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <time.h>
#include <algorithm>

// g++ -std=c++0x dens.cpp 

using namespace std;
typedef vector<vector<double> > Matrix;
typedef vector<vector<vector<double> > > ThDMtx;


Matrix LoadMatrix(int n, int m, string szo, Matrix M)
	{
	ifstream fajl (szo.c_str());
	if (!fajl.is_open())
		{
		cout << "nem sikerült megnyitni a fajlt!" << endl;
		}
	
	double szam;
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m; ++l)
			{
			fajl >> szam;
			M[k][l] = szam;
			//cout << M[k][l] << "; " << k << "; " << l << endl;
			}
		}
	fajl.close();
	return M;
	}

Matrix createMatrix(int n, int m)
	{
	Matrix eredmeny(n);
	
	for (int i = 0; i < n; ++i)
		eredmeny[i].resize(m);
	return eredmeny;
	}

/*3DMtx create3DMtx(int n, int m, int k)
	{
	
	}*/

double Average(vector<double> v)
{      double sum=0;
       for(int i=0;i<v.size();i++)
               {
               sum = sum + v[i];
               //cout << v[i] << "; " << sum << endl;
               }
       //cout << v[4] << "; " << v.size() << "; " << sum/v.size() << endl;
       return sum/v.size();
}

double Deviation(vector<double> v, double ave)
{
    double E=0;
    // Quick Question - Can vector::size() return 0?
    double inverse = 1.0 / static_cast<double>(v.size());
    for(unsigned int i=0;i<v.size();i++)
    {
        E += pow(static_cast<double>(v[i]) - ave, 2);
    }
    return sqrt(inverse * E);
}

int main()
	{
	
	
	int t = 1000000;
	int N = 50;
	
	
	chdir("abrak");
	
	vector<double> v;
	vector<double> v2;
	
	Matrix M (t,vector <double>(3,0));
	M = LoadMatrix(t,3,"ising1.6.dat",M);
	
		
	for (int t1 = 100000; t1 < t; t1++)
		{
		v.push_back(M[t1][1]);
		v2.push_back(M[t1][2]);
		
		}
		
	double em = Average(v);
	double dev = Deviation(v, em);
	double em2 = Average(v2);
	double dev2 = Deviation(v2, em2);
	
	cout << em << "; " << dev << "; " << em2 << "; " << dev2 << endl;

	return 0;
	}

