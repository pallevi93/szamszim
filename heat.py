from __future__ import division
from numpy import *
import numpy as np
import os

def save(t, Nx, Ny, T):
	f = open(str(t)+'.dat', 'w')
	for i in range (0,Nx):
		for j in range (0,Ny):
			f.write(str(i)+' '+str(j)+' '+str(T[i,j,0])+'\n')
	f.close()

def Temp(i, j, T, cons, consh, Te, x1, x2, y1, y2):
	if x1==1:
		a = T[i+1,j,0]
	else:
		a = 0
	if x2==1:
		b = T[i-1,j,0]
	else:
		b = 0
	if y1==1:
		c = T[i,j+1,0]
	else:
		c = 0
	if y2==1:
		d = T[i,j-1,0]
	else:
		d = 0
		
	T[i,j,1] = T[i,j,0] + cons*(a + b + c + d - (x1 + x2 + y1 + y2)*T[i,j,0]) - consh*(T[i,j,0]-Te)
	return T[i,j,1]

def Heat(Hepx1, Hepx2, Hepy1, Hepy2, He):
	for i in range (Hepx1,Hepx2+1):
		for j in range (Hepy1,Hepy2+1):
			T[i,j,0] = He

Nx=130; Ny=10; Nt=10000; Dx=0.0025; Dt=0.01
KAPPA=149.; SPH=710.; RHO=2328.; T0=293.; h=200
#KAPPA=401.; SPH=390.; RHO=8940.; T0=293.; h=0.
#KAPPA2=149.; SPH2=710.; RHO2=2328.; T02=293.; h2=200
KAPPA2=401.; SPH2=390.; RHO2=8940.; T02=293.; h2=10.
Te=293.
Hes=1; Hec=1; He1=413; He2=77
He1px1=4; He1px2=5; He1py1=4; He1py2=5
He2px1=86; He2px2=91; He2py1=47; He2py2=52
mappa = '19'; kiir=100

T=np.zeros((Nx,Ny,2))
D1=np.zeros((Nx,4))
avgT=np.zeros((Nt,3))
avgT2=np.zeros((Nt,3))

cons1 = KAPPA/(SPH*RHO)*Dt/(Dx*Dx)
cons2 = KAPPA2/(SPH2*RHO2)*Dt/(Dx*Dx)
cons3 = (h*Dt*2)/(Dx*RHO*SPH)
cons4 = (h2*Dt*2)/(Dx*RHO2*SPH2)

os.mkdir(mappa)
os.chdir(mappa)

KF = np.array([["Nx", Nx], ["Nt", Nt], ["Dx", Dx], ["Dt", Dt], ["kiir",kiir], ["Te",Te], ["KAPPA",KAPPA], ["SPH",SPH], ["RHO",RHO], ["T0",T0], ["h",h], ["cons1",cons1], ["cons3",cons3], ["KAPPA2",KAPPA2], ["SPH2",SPH2], ["RHO2",RHO2], ["T02",T02], ["h2",h2], ["cons2",cons2], ["cons4",cons4], ["Hes",Hes], ["Hec",Hec], ["He1",He1], ["He1px1",He1px1], ["He1px2",He1px2], ["He1py1",He1py1], ["He1py2",He1py2], ["He2",He2], ["He2px1",He2px1], ["He2px2",He2px2], ["He2py1",He2py1], ["He2py2",He2py2] ])

for i in range (0,Nx):
	for j in range (0,Ny):
		if i < Nx/2:
			T[i,j,0] = T0
		else:
			T[i,j,0] = T02

if Hes != 0:
	if Hec == 0:
		Heat(He1px1, He1px2, He1py1, He1py2, He1)
		
		if Hes == 2:
			Heat(He2px1, He2px2, He2py1, He2py2, He2)

np.savetxt("KF", KF, delimiter='\t', fmt="%s")
save(0, Nx, Ny, T)
t2=0
z=1
o=1
Tavg=0
Tavg2=0
for i in range (0,Nx):
	for j in range (0,Ny):
		if i < Ny:
			Tavg2 = Tavg2 + T[i,j,0]
		Tavg = Tavg + T[i,j,0]
avgT[0,1]= Tavg/(Nx*Ny)
avgT2[0,1]= Tavg2/(Ny*Ny)

for t in range (1,Nt):
	if Hes != 0:
		if Hec != 0:
			Heat(He1px1, He1px2, He1py1, He1py2, He1)
			
			if Hes == 2:
				Heat(He2px1, He2px2, He2py1, He2py2, He2)
	
	Tavg=0
	Tavg2=0
	for i in range (0,Nx):
		for j in range (0,Ny):
			#if i < Nx/2:
			if i < Ny:
				cons = cons1
				consh = cons3
			else:
				cons = cons2
				consh = cons4
		
			if i!=0 and j!=0 and i!=Nx-1 and j!=Ny-1:
				T[i,j,1] = Temp(i, j, T, cons, consh, Te, 1, 1, 1, 1)
			
			elif i==0 and j!=0 and j!=Ny-1:
				T[i,j,1] = Temp(i, j, T, cons, consh, Te, 1, 0, 1, 1)

			elif i!=0 and j==0 and i!=Nx-1:
				T[i,j,1] = Temp(i, j, T, cons, consh, Te, 1, 1, 1, 0)
				
			elif j!=0 and i==Nx-1 and j!=Ny-1:
				T[i,j,1] = Temp(i, j, T, cons, consh, Te, 0, 1, 1, 1)
				
			elif i!=0 and i!=Nx-1 and j==Ny-1:
				T[i,j,1] = Temp(i, j, T, cons, consh, Te, 1, 1, 0, 1)
				
			elif i==0 and j==0:
				T[i,j,1] = Temp(i, j, T, cons, consh, Te, 1, 0, 1, 0)
				
			elif i==0 and j==Ny-1:
				T[i,j,1] = Temp(i, j, T, cons, consh, Te, 1, 0, 0, 1)
			
			elif j==0 and i==Nx-1:
				T[i,j,1] = Temp(i, j, T, cons, consh, Te, 0, 1, 1, 0)

			elif i==Nx-1 and j==Ny-1:
				T[i,j,1] = Temp(i, j, T, cons, consh, Te, 0, 1, 0, 1)
			
			Tavg = Tavg + T[i,j,1]
			if i < Ny:
				Tavg2 = Tavg2 + T[i,j,1]
	
	
	if kiir==z:
		t2=t2+1
		for i in range (0,Nx):
			for j in range (0,Ny):
				if j==(Ny/2)-1:
					D1[i,0] = i
					D1[i,1] = T[i,j,1]
					D1[i,2] = (T[i,j,1] - T[i,j,0])/Dt
					if i!=Nx-1:
						D1[i,3] = (T[i,j,1] - T[i+1,j,1])/Dx
					else:
						D1[i,3] = D1[i-1,3]
		np.savetxt(str(t2)+"_D1.txt", D1)
		save(t2, Nx, Ny, T)
		z=0
	z=z+1
	
	
	for i in range (0,Nx):
		for j in range (0,Ny):			
			T[i,j,0] = T[i,j,1]
		
	avgT[t,0]= t*Dt
	avgT2[t,0]= t*Dt
	avgT[t,1]= Tavg/(Nx*Ny)
	avgT2[t,1]= Tavg2/(Ny*Ny)
	avgT[t,2]= (avgT[t,1]-avgT[t-1,1])/Dt
	avgT2[t,2]= (avgT2[t,1]-avgT2[t-1,1])/Dt
		
	if Nt*o*0.1==t:
		print str(o*10)+"%"
		o=o+1
		
savetxt("Tavg.txt",avgT)
savetxt("Tavg2.txt",avgT2)
