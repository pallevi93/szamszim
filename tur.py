from __future__ import division
import turtle
import numpy as np
import random
import shapely.geometry as geometry
import math

def get_triangle(n):  #definialok egy egyenloszaru haromszoget
	ax = ay = 0.0
	a = ax, ay

	bx = 0.5  *  n
	by = math.sqrt( n**2 -(n/2)**2)
	b = bx, by

	cx = n
	cy = 0.0
	c = cx, cy

	triangle = geometry.Polygon([a, b, c])
	return triangle

def midpoint(point1, point2): #megadja ket pont altal alkotott szakasz felezopontjat
	#print point1, point2
	return ((point1.x + point2[0])/2, (point1.y + point2[1])/2)

def chaos(steps,n):
	#t.screen.setworldcoordinates(-n*0.1, -n*0.1, n*1.1,n*1.1) #beallitom a kepernyot h kozepen legyen es ne menjen ki a haromszog belole
	#megsem jo mert eltorzitja a kepet
	
	triangle=get_triangle(n) #letrehozom a haromszoget
	point = geometry.Point(0, 0) #felveszek egy pontot
	j=False
	while (j==False): #addig generalok egy random pontot egy negyzetben aminek akkora az oldala mint a haromszognek es a haromszog benne van a negyzetben, amig az beleesik a haromszogbe
		x = random.uniform(0, n)
		y = random.uniform(0, n)
		point = geometry.Point(x, y)
		j = point.within(triangle)
		
	#print str(point.x)+' '+str(point.y)
	
	t.pu()  #megrajzolom a haromszoget csak szemleltetesbol (a tollam fel van emelve)
	t.fd(n)
	t.lt(120)
	t.fd(n)
	t.lt(120)
	t.fd(n)
	t.setpos(point.x,point.y) #felveszem az elobb kiszamolt random pontot

	for i in range(0,steps):  	#ahany step annyi pontot generealok
		val = random.randint(0,2) #generalok egy random szamot (0,1,2) h melyik csucsal kotom ossze a pontomat es veszem e ket pont altal alkotott szakasz felezo pontjat
		if val == 0:
			point = geometry.Point(midpoint(point, triangle.exterior.coords[0]))
		if val == 1:
			point = geometry.Point(midpoint(point, triangle.exterior.coords[1]))
		if val == 2:
			point = geometry.Point(midpoint(point, triangle.exterior.coords[2]))
		#print point
		t.setpos(point.x,point.y) #ez a pont az uj pontom
		#t.dot() #rajzolok egy pontot
		t.dot(2)
		
def arrowhead_curve1(level,parity,length):
	if level:  #ha a level nem 0
		level-=1 #1-el kisebb lesz a level
		arrowhead_curve1(level,-parity,length)	#meghivom megint fv-t kisebb lvl-el es ellentetes iranyba
		t.lt(parity*60) 	#jobbra vagy balar fordulok attol fuggoen h parity=-1 vagy 1
		arrowhead_curve1(level,parity,length)
		t.lt(parity*60)	
		arrowhead_curve1(level,-parity,length)
	else:	#ha a lvl 0 akkor megyek 10-et
		t.fd(length)

def arrowhead_curve(level,parity,length):
	#t.screen.setworldcoordinates(-2**level*length*0.1, -2**level*length*0.1, 2**level*length*1.1,2**level*length*1.1)
	#turtle.screensize(2000,2000)
	if (level % 2) != 0:
		t.lt(60)
	arrowhead_curve1(level,parity,length)
	
window = turtle.Screen()
turtle.screensize(2000,2000)
t = turtle.Turtle()
t.speed(0) #a turtle sebessege
#t.ht()

#lehet valasztani egy szimpatikus fv-t

chaos(50000,600)		#hany pontot rajzoljon, es mekkora legyen a 3szog elenek hossza
#arrowhead_curve(9,-1,0.9765)	#hanyadik szintig csinalja, a -1 a paritas azt nem kell megvaltoztatni (a program meghivja onmagat azert fontos), mennyit menjen a turtle mielott kanyarodik


t.ht()  #elrejtem a turtlet
raw_input("Set your image to the center and press the <ENTER> key to continue")
turtle.getcanvas().postscript(file="50000_4.eps")  #kiirom egy fileba a kepet

window.exitonclick()


