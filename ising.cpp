#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <time.h>
#include <algorithm>
#include <math.h>
#include <sys/stat.h>
#include <cstdlib>
#include <ctime>
#include <iterator>


using namespace std;
typedef vector<vector<double> > Matrix;

double DE(int xi, int xip1, int xim1)
	{
	double de;
	de = 2 * xi * (xim1 + xip1);
	return de;
	}
	
int XRand()
	{
	int x;
	int randomval = rand() % 2;
	if (randomval == 0)
		x = -1;
	else
		x = 1;
	return x;
	}
	
double fRand(double fMin, double fMax)
	{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
	}
	
void SaveMatrix(int n, int m, string szo, Matrix& M)
	{
	ofstream file (szo.c_str());
	if (!file)
		{
		cerr << "Cannot open " << "d.dat" << "\nExiting ...\n";
		}
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m-1; ++l)
			{
			file << M[k][l] << '\t';
			}
		file << M[k][m-1] << '\n';
		}
	file.close();
	}
	
Matrix createMatrix(int n, int m)
	{
	Matrix eredmeny(n);
	
	for (int i = 0; i < n; ++i)
		eredmeny[i].resize(m);
	return eredmeny;
	}
	
double Average(vector<double> v)
{      double sum=0;
       for(int i=0;i<v.size();i++)
               {
               sum = sum + v[i];
               //cout << v[i] << "; " << sum << endl;
               }
       //cout << v[4] << "; " << v.size() << "; " << sum/v.size() << endl;
       return sum/v.size();
}

double Deviation(vector<double> v, double ave)
{
    double E=0;
    // Quick Question - Can vector::size() return 0?
    double inverse = 1.0 / static_cast<double>(v.size());
    for(unsigned int i=0;i<v.size();i++)
    {
        E += pow(static_cast<double>(v[i]) - ave, 2);
    }
    return sqrt(inverse * E);
}
	
int main()
	{
	srand(time(0));
	int N = 50;
	double P;
	double BJ = 1.6;
	double de;
	int t = 1000000;
	
	vector<double> v(N);
	vector<double> m(t);
	vector<double> m2(t);
	double dev;
	double dev2;
	
	//Matrix r = createMatrix(N, 2);
	
	mode_t mt;
	mkdir("1.6,2", mt);
	chdir("1.6,2");
	for (int f = 0; f < 100; f++)
		{
		for (int n = 0; n < N; n++)
			{
			//v[n] = XRand();
			v[n] = -1;
			}
		
		string chd = to_string(f) + "ising.dat";	
		ofstream file(chd.c_str());
		if (!file)
			{
			cerr << "Cannot open " << "d.dat" << "\nExiting ...\n";
			return 1;
			}
		for (int t1 = 0; t1 < t; t1++)
			{
			for (int n = 0; n < N; n++)
				{
				int j = rand() % 50;
				if (j == 0)
					de = DE(v[j], v[j+1], 0);
				if (j == (N-1))
					de = DE(v[j], 0, v[j-1]);
				if (j > 0 && j < (N-1))
					de = DE(v[j], v[j+1], v[j-1]);
		
				if (de < 0)
					v[j] = v[j] * -1;
				if (de == 0)
					{
					P = fRand(0, 1);
					if (P < 1/2)
						v[j] = v[j] * -1;
					}
				if (de > 0)
					{
					P = fRand(0, 1);
					if ( P < (exp(BJ * (-1) * de )) )
						v[j] = v[j] * -1;
					}
				}
			m[t1] = Average(v);
			dev = Deviation(v, m[t1]);
			m2[t1] = m[t1] * m[t1];
			dev2 = Deviation(v, m2[t1]);
		
			file << t1 << '\t' << m[t1] << '\t' << dev << '\t' << m2[t1] << '\t' << dev2 << endl;
			}
		file.close();
		}
	//cout << Average(m) << endl;
	return 0;
	}
