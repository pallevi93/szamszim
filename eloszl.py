import os
import sys
import numpy as np


f='usa'
mappa=f
os.chdir(mappa)

s=np.loadtxt(f+"_trace.txt")
n = [[0,0] for n in range(0,30)]
for j in range(1,31):
	d=0
	for i in range(0, len(s)):
		if j==s[i]:
			d=d+1
	n[j-1][0]=j
	n[j-1][1]=d
np.savetxt(mappa+"_eloszl.txt", n)
