#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <time.h>
#include <algorithm>
#include <sys/stat.h>

// g++ -std=c++0x dens.cpp 

using namespace std;
typedef vector<vector<double> > Matrix;
typedef vector<vector<vector<double> > > ThDMtx;


double Average(vector<double> v)
{      double sum=0;
       for(int i=0;i<v.size();i++)
               {
               sum = sum + v[i];
               //cout << v[i] << "; " << sum << endl;
               }
       //cout << v[4] << "; " << v.size() << "; " << sum/v.size() << endl;
       return sum/v.size();
}
//DEVIATION
double Deviation(vector<double> v, double ave)
{
    double E=0;
    // Quick Question - Can vector::size() return 0?
    double inverse = 1.0 / static_cast<double>(v.size());
    for(unsigned int i=0;i<v.size();i++)
    {
        E += pow(static_cast<double>(v[i]) - ave, 2);
    }
    return sqrt(inverse * E);
}

void LoadMatrix(int n, int m, string szo, Matrix& M)
	{
	ifstream fajl (szo.c_str());
	if (!fajl.is_open())
		{
		cout << "nem sikerült megnyitni a fajlt!" << endl;
		}
	
	double szam;
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m; ++l)
			{
			fajl >> szam;
			M[k][l] = szam;
			//cout << M[k][l] << "; " << k << "; " << l << endl;
			}
		}
	fajl.close();
	}
	
void SaveMatrix(int n, int m, string szo, Matrix& M)
	{
	ofstream file (szo.c_str());
	if (!file)
		{
		cerr << "Cannot open " << "d.dat" << "\nExiting ...\n";
		}
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m-1; ++l)
			{
			file << M[k][l] << '\t';
			}
		file << M[k][m-1] << '\n';
		}
	file.close();
	}
Matrix createMatrix(int n, int m)
	{
	Matrix eredmeny(n);
	
	for (int i = 0; i < n; ++i)
		eredmeny[i].resize(m);
	return eredmeny;
	}


int main()
	{
	int s = 0;
	int x = 5001-2;
	int N = 2;
	
	chdir("1");
	Matrix KCS2 (x,vector <double>(4,0));
	Matrix KCS (x-4,vector <double>(6,0));
	vector<double> vfoksz;		
	while (N<5001)
		{
		Matrix H = createMatrix(N, N);
		cout << N << endl;
		
		Matrix Pk (N-1,vector <double>(4,0));
		LoadMatrix(N, N, "B_A,1," + to_string(N) + ".txt", H);
		for (int i = 0; i < N; i++)
			{
			double foksz = 0;
			for (int j = 0; j < N; j++)
				{
				if (i!=j)
					{
					if (H[i][j] == 1)
						{
						foksz++;
						}
					}
				}
			vfoksz.push_back (foksz);
			}
		double AVG1 = Average(vfoksz); 
		double dev1 = Deviation(vfoksz,AVG1); 
		KCS2[N-2][1] = AVG1;
		KCS2[N-2][2] = dev1;
		KCS2[N-2][3] = dev1/sqrt(vfoksz.size());
		KCS2[N-2][0] = N;
		for (int k = 1; k <= N-1; k++)
			{
			double z = count (vfoksz.begin(), vfoksz.end(), k);
			Pk[k-1][1] = z/N;
			Pk[k-1][0] = k;
			Pk[k-1][2] = exp(-k*log(2));
			Pk[k-1][3] = abs(1-(Pk[k-1][1]/exp(-k*log(2))));
			}
			
		SaveMatrix(N-1,4, "Pk,"+ to_string(N) + ".txt", Pk); 
		
		if (N>5)
			{
			KCS[N-6][0] = N;
			KCS[N-6][1] = Pk[4][3];
			KCS[N-6][2] = Pk[3][3];
			KCS[N-6][3] = Pk[2][3];
			KCS[N-6][4] = Pk[1][3];
			KCS[N-6][5] = Pk[0][3];
			}
		
		vfoksz.clear();
		N++;
		}
	SaveMatrix(x,4,"avg.txt",KCS2);
	SaveMatrix(x-4,6,"hiba.txt",KCS);
	return 0;
	}
