#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <time.h>
#include <algorithm>

// g++ -std=c++0x dens.cpp 

using namespace std;
typedef vector<vector<double> > Matrix;
typedef vector<vector<vector<double> > > ThDMtx;


Matrix LoadMatrix(int n, int m, string szo, Matrix M)
	{
	ifstream fajl (szo.c_str());
	if (!fajl.is_open())
		{
		cout << "nem sikerült megnyitni a fajlt!" << endl;
		}
	
	double szam;
	for (int k = 0; k < n; ++k)
		{
		for (int l = 0; l < m; ++l)
			{
			fajl >> szam;
			M[k][l] = szam;
			//cout << M[k][l] << "; " << k << "; " << l << endl;
			}
		}
	fajl.close();
	return M;
	}

Matrix createMatrix(int n, int m)
	{
	Matrix eredmeny(n);
	
	for (int i = 0; i < n; ++i)
		eredmeny[i].resize(m);
	return eredmeny;
	}

/*3DMtx create3DMtx(int n, int m, int k)
	{
	
	}*/

double Average(vector<double> v)
{      double sum=0;
       for(int i=0;i<v.size();i++)
               {
               sum = sum + v[i];
               //cout << v[i] << "; " << sum << endl;
               }
       //cout << v[4] << "; " << v.size() << "; " << sum/v.size() << endl;
       return sum/v.size();
}

double Deviation(vector<double> v, double ave)
{
    double E=0;
    // Quick Question - Can vector::size() return 0?
    double inverse = 1.0 / static_cast<double>(v.size());
    for(unsigned int i=0;i<v.size();i++)
    {
        E += pow(static_cast<double>(v[i]) - ave, 2);
    }
    return sqrt(inverse * E);
}

int main()
	{
	clock_t t0 = clock();
	clock_t td;
	
	int t = 400;
	int N = 50;
	
	
	chdir("0.1");
	
	Matrix m(t,vector <double>(100,0));
	Matrix m2 (t,vector <double>(100,0));
	
	Matrix M (t,vector <double>(3,0));
	for (int f = 0; f < 100; f++)
		{
		
		
		string chd = to_string(f)+ "ising.dat";
		
		Matrix cp (t,vector <double>(5,0));
		
		cp = LoadMatrix(t, 5, chd, cp);
		
		for (int t1 = 0; t1 < t; t1++)
			{
			M[t1][1] = M[t1][1] + cp[t1][1];
			M[t1][2] = M[t1][2] + cp[t1][3];
			
			m[t1][f] = cp[t1][1];
			m2[t1][f] = cp[t1][3];
			}
		}
	
	ofstream file("ising0.1.dat");
	if (!file)
		{
		cerr << "Cannot open " << "d.dat" << "\nExiting ...\n";
		return 1;
		}
	file << 0 << '\t' << -1 << '\t' << 1 << '\n';
	for (int t1 = 0; t1 < t; t1++)
		{
		M[t1][1] = M[t1][1] /100;
		M[t1][2] = M[t1][2] /100;
		
		file << t1+1 << '\t' << M[t1][1] << '\t' << M[t1][2] << '\n';
		}
	file.close();
	
	ofstream file2("ising0.1,2.dat");
	if (!file2)
		{
		cerr << "Cannot open " << "d.dat" << "\nExiting ...\n";
		return 1;
		}
	for (int t1 = 0; t1 < t; t1++)
		{
		double em = Average(m[t1]);
		double dev = Deviation(m[t1], em);
		double em2 = Average(m2[t1]);
		double dev2 = Deviation(m2[t1], em2);
		
		file2 << t1 << '\t' << em << '\t' << dev << '\t' << em2 << '\t' << dev2 << '\n';
		}
	file2.close();
		
	td = clock() - t0;
	cout << ((double)td)/CLOCKS_PER_SEC << endl;
	return 0;
	}

